<?php

namespace App\Http\Controllers;

use App\Mail\webmail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use Mail;

class PostController extends Controller
{
    public function getDashboard()
    {
        return view('admin.dashboard');
    }
    public function postMessage(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'email' => 'required',
            'title' => 'required',
            'message' => 'required'
        ]);
        $nom = $request['nom'];
        $email = $request['email'];
        $sujet = $request['sujet'];
        $mess = $request['title'];

        $data =array(
            'email' =>$request->email,
            'sujet' =>$request->subject,
            'title' =>$request->title
        );
            Mail::send('mymail', $data, function($message) use ($data){
            $message->from($data['email']);
            $message->to('geniustechsn@gmail.com');
            $message->subject($data['sujet']);
        });




    }
}
