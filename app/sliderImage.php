<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sliderImage extends Model
{
    protected $table = 'slider_images';
    protected $fillable = [
        'titre',
        'description',
        'image'
    ];
}
