<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- For-Mobile-Apps-and-Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //For-Mobile-Apps-and-Meta-Tags -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>
<div class="container">
        <h1><img src="images/3.png" width="70px" height="70px">Authentification</h1>
    <div class="contact">

        <div class="right-w3">
            <p class="para">Genius Tech.</p>
            <p>Dakar,</p>
            <p>Senegal.</p>
            <p>Telephone : +221 77 809 27 66</p>
            <a href="mailto:info@example.com">Email : genius@geniutech.sn</a>
            <h2>Administration</h2>
        </div>
        @include('admin.includes.message-block')
        <form action="{{ route('signin') }}" method="post">
            {{ csrf_field() }}
            <div class="left">

                <div class="email">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <input type="text" name="email" id="email" class="email" placeholder="Votre Email" required="">
                </div>
                <div class="password">
                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                    <input type="password" name="password" id="password" class="phone" placeholder="Votre Mot De Passe" required="">
                    <input class="form-control" name="_token" value ="{{ Session::token() }}" type="hidden">
                </div>
                <input type="submit" value="Send">
            </div>
        </form>

        <div class="clear"></div>
    </div>
    <div class="footer-w3l">
        <p class="agileinfo"> 2017 Agil.</p>
    </div>

</div>
</body>
</html>