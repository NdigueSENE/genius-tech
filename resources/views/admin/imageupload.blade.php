<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</head>
<body>
<h1 class="well well-lg">Upload Image</h1>
<div class="container">
    <div class="col-md-6">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>

        @endif
        <h3>S'inscrire</h3>
        <form action="{{ route('imageUploadForm') }}"  enctype="multipart/form-data"  method="post">
            <div class="form-group">
                <label for="titre">Titre</label>
                <input class="form-control"  type="text" name="titre" id="titre" value="">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description"></textarea>
            </div>

            <div class="form-group">
                <label for="image">Fichier</label>
                <input type="file" name="image" />
            </div>

            <button class="btn btn-primary" type="submit"> Enregistrer </button>
            <input class="form-control" name="_token" value ="{{ Session::token() }}" type="hidden">
        </form>
    </div>

</div>
</body>
</html>