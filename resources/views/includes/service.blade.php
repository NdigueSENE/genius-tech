<section id="services">
    <div class="container">
        <div class="heading wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="row">
                <div class="text-center col-sm-8 col-sm-offset-2">
                    <h2>NOS SERVICES</h2>

                </div>
            </div>
        </div>
        <div class="text-center our-services">
            <div class="row">
                <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <div class="service-icon">
                        <i class="fa fa-desktop"></i>
                    </div>
                    <div class="service-info">
                        <h3>Développement WEB</h3>
                        <p>Application web - Portail web - Site e-commerce - Site vitrine </p>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
                    <div class="service-icon">
                        <i class="fa fa-mobile fa-2x"></i>
                    </div>
                    <div class="service-info">
                        <h3>Développement MOBILE</h3>
                        <p>Android - Native - Quasi-Native - Windows Phone - IOS
                           </p>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="550ms">
                    <div class="service-icon">
                        <i class="fa fa-flask"></i>
                    </div>
                    <div class="service-info">
                        <h3>Solution sur mesure</h3>
                        <p>Nous proposons des solutions adaptées aux entreprises et PME à des prix imbattables</p>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="650ms">
                    <div class="service-icon">
                        <i class="fa fa-signal"></i>
                    </div>
                    <div class="service-info">
                        <h3>RESEAUX</h3>
                        <p>Administration Windows Serveurs / Linux - Cablâge - Services Réseaux (serveur de messagerie, DNS, DHCP...) </p>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="750ms">
                    <div class="service-icon">
                        <i class="fa fa-connectdevelop"></i>
                    </div>
                    <div class="service-info">
                        <h3>TELECOMMUNICATIONS</h3>
                        <p>Téléphonie sur IP (ToIP, VoIP...)</p>
                    </div>
                </div>
                <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="850ms">
                    <div class="service-icon">
                        <i class="fa fa-key"></i>
                    </div>
                    <div class="service-info">
                        <h3>TRANSFORMATION DIGITALE</h3>
                        <p>Nous accompagnons les entreprises et PME tout au long de leurs processus de transformation digitale.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--/#services-->