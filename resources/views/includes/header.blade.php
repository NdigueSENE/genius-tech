<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Genius Tech</title>
    <link href="{{URL::to ('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{URL::to ('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{URL::to ('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{URL::to ('css/lightbox.css') }}" rel="stylesheet">
    <link href="{{URL::to ('css/main.css') }}" rel="stylesheet">
    <link id="css-preset" href="{{URL::to ('css/presets/preset1.css') }}" rel="stylesheet">
    <link href="{{URL::to ('css/responsive.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="{{URL::to('js/html5shiv.js') }}"></script>
    <script src="{{URL::to ('js/respond.min.js') }}"></script>
    <![endif]-->

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{URL::to ('images/logo.png') }}">
</head><!--/head-->
<body>

<!--.preloader-->
<div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
<!--/.preloader-->